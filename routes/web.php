<?php

Auth::routes();


//Controladores inicio
Route::get('/', 'MainController@home');
Route::get('/home', 'HomeController@index');


//Controladores principales
Route::get('/invoices/edit/{weeks_id}/{plant_id}', ['as' => 'invoice_edit_by_plant', 'uses' => 'InvoicesController@edit_by_plant']);
Route::post('/invoices/edit/{week}/{plant}', ['as' => 'invoice.update_by_plant', 'uses' => 'InvoicesController@update_by_plant']);


Route::get('tokens/crear', ['as' => 'tokens-crear', 'uses' => 'TokensController@tokens_crear_base']);
Route::post('tokens/crear', ['as' => 'tokens-crear-post', 'uses' => 'TokensController@tokens_crear_post']);
Route::get('tokens/actualizar/{id}/{step?}', ['as' => 'tokens-update-base', 'uses' => 'TokensController@tokens_update_base']);
Route::post('tokens/actualizar/{id}/{step?}', ['as' => 'tokens-update-post', 'uses' => 'TokensController@tokens_update_post']);

Route::resource('tokens', 'TokensController');
Route::resource('plants', 'PlantsController');
Route::resource('drivers', 'DriversController');
Route::resource('invoices', 'InvoicesController');

//Controladores auxiliares
Route::resource('zones', 'ZonesController');
Route::resource('zonesa', 'ZonesaController');
Route::resource('centers', 'CentersController');
Route::resource('weeks', 'WeeksController');




//Rutas imagenes plantas
Route::get('plants/images/{filename}', function ($filename){

    $path = storage_path("app/images/$filename");

    if (!\File::exists($path)) abort(404);
    $file = \File::get($path);
    $type = \File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});



//Rutas imagenes operadores
Route::get('drivers/images/{filename}', function ($filename){

    $path = storage_path("app/images/$filename");

    if (!\File::exists($path)) abort(404);
    $file = \File::get($path);
    $type = \File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/getZonasB_ByPlanta', ['as' => 'getZonasB_ByPlanta', 'uses' => 'PlantsController@getZonasB_ByPlanta']);
