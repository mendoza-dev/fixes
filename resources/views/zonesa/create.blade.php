@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Zona A            <small>Nueva</small></h1>
        </div><br>
        <div class="well well-lg">
            <div class="row">
                @include('zonesa.form',['zonea' => $zonea, 'url' => '/zonesa', 'method' => 'POST'])
            </div>
        </div>
    </div>


@endsection
