{!! Form::open (['url' => $url, 'method' => $method]) !!}
<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('number', $week->number,['class'=> 'form-control','placeholder'=>'Nombre', 'title'=>"Nombre", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('date_start', $week->date_start,['class'=> 'form-control','placeholder'=>'Inicio', 'title'=>"Inicio", 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('date_end', $week->date_end,['class'=> 'form-control','placeholder'=>'Fin', 'title'=>"Fin", 'required' => 'required']) }}
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-1 col-md-offset-9" align="">
        <a class="btn btn-warning" href="{{ url('/weeks') }}" role="button">Cancelar</a>
    </div>
    <div class="col-sm-1" align="">
        <input class="btn btn-success" type="submit" value="Guardar">
    </div>
</div>

{!! Form::close ()!!}
