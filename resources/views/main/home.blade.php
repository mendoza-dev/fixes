@extends('layouts.app')

@section('content')

    <div class="content container">
        <div class="jumbotron">
            <h1>Grupo fixes</h1>
            <p>Control operativo-administrativo</p>

            <p><a class="btn btn-primary" href="{{ url('/login') }}" role="button">Inicia</a></p>
        </div>
    </div>

@endsection