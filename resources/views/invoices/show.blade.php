@extends('layouts.app')
@section('content')


<div class="content container">
    <section class="widget">
    <div class="body no-margin">
        <div class="row">
            <div class="page-header">
                <h1 style="color: white">Factura                                                    <small>detalle</small></h1>
            </div>
        </div>
        @foreach($invoice->getTokens as $token)
        <section class="invoice-info well">
            <div class="row">
                <div class="col-sm-6 col-print-6">
                    <h4 class="details-title">Empresa remitemte</h4>
                    <h3 class="company-name">
                        Grupo Fixes Industrial
                    </h3>
                    <address>
                        <strong>GRUPO FIXES INDUSTRIAL S.A. DE C.V.</strong><br>
                        GFI130805MX1<br>
                        PERSONA MORAL<br>
                        <small>JARDINES DE LAS ALAMEDAS 223 JARDINES DE JACARANDAS 78390<br>
                        SAN LUIS POTOSI MEXICO</small>
                    </address>
                </div>
                <div class="col-sm-6 col-print-6 client-details">
                    <h4 class="details-title">Empresa destinataria</h4>
                    <h3 class="client-name">
                        {{ $token->plant->name }}

                    </h3>
                    <address>
                        <strong>{{ $token->plant->social_reason }}</strong><br>
                        {{ $token->plant->rfc }}<br>
                        <small>{{ $token->plant->street }}{{ $token->plant->ext_number }}{{ $token->plant->suburb }}{{ $token->plant->cp }}<br>
                        {{ $token->plant->city }} MÉXICO</small>


                    </address>
                </div>
            </div>
        </section>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Planta</th>
                <th>Fecha</th>
                <th>Zona B</th>
                <th>Importe</th>

            </tr>
            </thead>
            <tbody>

                <tr>
                    <td>{{ $token->plant->name }}</td>
                    <td>{{ $token->date }}</td>
                    <td>{{ $token->getZoneB->name }}</td>
                    <td>${{ number_format($token->cost_bill, 2) }}</td>
                </tr>
            @endforeach
            </tbody>

        </table>
        <div class="row">

                    <div class="col-sm-3 col-sm-offset-9">
                        <div class="alert alert-info">

                            Total ${{ number_format($token->sum('cost_bill'), 2) }}
                        </div>


                    </div>


                </div>


        </div>
    </div>
</section>
</div>

@endsection