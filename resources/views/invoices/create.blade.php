@extends('layouts.app')

@section('content')
    <div class="content container">
        <div class="page-header">
            <h1 style="color: white">Facturas                                                    <small>Nueva</small></h1>
        </div><br>
        <div class="row">
            @include('invoices.form')
        </div>
    </div>
@stop