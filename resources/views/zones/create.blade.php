@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Zona B            <small>Nueva</small></h1>
        </div><br>
        <div class="well well-lg">
                <div class="row">
                    @include('zones.form',['zone' => $zone, 'url' => '/zones', 'method' => 'POST'])
                </div>
            </div>
        </div>


@endsection
