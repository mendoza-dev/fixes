<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('Grupo Fixes', 'Grupo Fixes') }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/slate/bootstrap.min.css" rel="stylesheet" integrity="sha384-RpX8okQqCyUNG7PlOYNybyJXYTtGQH+7rIKiVvg1DLg6jahLEk47VvpUyS+E2/uJ" crossorigin="anonymous">

    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Work',     11],
                ['Eat',      2],
                ['Commute',  2],
                ['Watch TV', 2],
                ['Sleep',    7]
            ]);

            var options = {
                title: 'Vales por planta'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }
    </script>





</head>
<body>


<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('Grupo Fixes', 'Grupo Fixes') }}
            </a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/home') }}">Home</a></li>
                <li class=" dropdown">
                    <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Plantas <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/plants') }}">Ver</a></li>
                        <li><a href="{{ url('/plants/create') }}">Crear</a></li>
                    </ul>
                </li>
                <li class=" dropdown">
                    <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Operadores<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/drivers') }}">Ver</a></li>
                        <li><a href="{{ url('/drivers/create') }}">Crear</a></li>
                    </ul>
                </li>
                <li class=" dropdown">
                    <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Vales<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/tokens/create') }}">Crear</a></li>
                        <li><a href="{{ url('/tokens') }}">Ver</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{url('/invoices')}}">Facturas</a>
                </li>
                <li class=" dropdown">
                    <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Configuracion<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/zones') }}">Zona B</a></li>
                        <li><a href="{{ url('/zonesa') }}">Zona A</a></li>
                        <li><a href="{{ url('/centers') }}">Centro de costos</a></li>
                        <li><a href="{{ url('/weeks') }}">Semanas</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Inicio</a></li>
                    <li><a href="{{ url('/register') }}">Registro</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Salir
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

@yield('content')

<!-- Scripts -->
<script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<script>
    $j = jQuery.noConflict();
    $j(function ($) {

        $('.plantaSelector').change(function(){
            $.get('{{ route('getZonasB_ByPlanta') }}', {planta:$(this).val()}, function(response){
                var htmlResponse = '';
                $.each(JSON.parse(response), function(i, data){
                    htmlResponse = htmlResponse + '<option value="'+ data['id'] + '">' + data['name'] + '</option>';
                });

                console.log(htmlResponse);
                $('.zonaBSelector').html(htmlResponse);
            });
        });

        $(document).ready(function () {
            var pasajeros = 0, zona_b = 0, tiempo = 0, otro_costo = 0, factura = 0;
        });

        $(document).ready(function () {
            var A = 1, B = 1;

        })

        $('.sumatoria').change(function(){
            pasajeros = $('#pasajeros').val()!=''?$('#pasajeros').val():0;
            pasajeros = pasajeros >= 1 ?(parseInt(pasajeros)-1):pasajeros;
            pasajeros = pasajeros >= 2 ?(parseInt(pasajeros)*50):pasajeros;
            zona_b = $('#zona_b').val()!=''? zonesB[$('#zona_b').val()]:0;
            tiempo = $('#tiempo').val()!=''?$('#tiempo').val():0;
            tiempo = tiempo = 1 ?(parseInt(tiempo)*5):tiempo;
            otro_costo = $('#otro_costo').val()!=''?$('#otro_costo').val():0;
            $('#factura').val(parseInt(pasajeros) + parseInt(zona_b) + parseInt(tiempo) + parseInt(otro_costo));
        });

        $( "#datepicker" ).datepicker({
            dateFormat:'yy-mm-dd'
        });


    });
</script>

<script>

    $(document).ready(function() {
        var activeSystemClass = $('.list-group-item.active');

        //something is entered in search form
        $('#system-search').keyup( function() {
            var that = this;
            // affect all table rows on in systems table
            var tableBody = $('.table-list-search tbody');
            var tableRowsClass = $('.table-list-search tbody tr');
            $('.search-sf').remove();
            tableRowsClass.each( function(i, val) {

                //Lower text for case insensitive
                var rowText = $(val).text().toLowerCase();
                var inputText = $(that).val().toLowerCase();
                if(inputText != '')
                {
                    $('.search-query-sf').remove();
                    tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Busqueda de: '
                            + $(that).val()
                            + '"</strong></td></tr>');
                }
                else
                {
                    $('.search-query-sf').remove();
                }

                if( rowText.indexOf( inputText ) == -1 )
                {
                    //hide rows
                    tableRowsClass.eq(i).hide();

                }
                else
                {
                    $('.search-sf').remove();
                    tableRowsClass.eq(i).show();
                }
            });
            //all tr elements are hidden
            if(tableRowsClass.children(':visible').length == 0)
            {
                tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">Sin resultados.</td></tr>');
            }
        });
    });
</script>

<script>
    $(document).ready(function(){
        var i=1;
        $("#add_row").click(function(){
            $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input name='name"+i+"' type='text' placeholder='Name' class='form-control input-md'  /> </td><td><input  name='mail"+i+"' type='text' placeholder='Mail'  class='form-control input-md'></td><td><input  name='mobile"+i+"' type='text' placeholder='Mobile'  class='form-control input-md'></td>");

            $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
            i++;
        });
        $("#delete_row").click(function(){
            if(i>1){
                $("#addr"+(i-1)).html('');
                i--;
            }
        });

    });

</script>

<script>
    $(document).ready(function () {
        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        });
        $(".prev-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
</script>
<script>
    $(document).ready(function(){
        var countToken = 1;
        $('.addRow').click(function(){
            countToken ++;
            var rowToken = '<hr><div class="row"><div class="col-sm-6"><div class="form-group"><div class="fg-line">' +
                    '<input class="form-control hasDatepicker" placeholder="Fecha de realizacion" title="Fecha de servicio" id="datepicker" required="required" name="token['+countToken+'][date]" value="2017-02-03" type="text">' +
                    '</div></div></div><div class="col-sm-6"><div class="form-group"><div class="fg-line">' +
                    '<input class="form-control" placeholder="Hora" title="Hora de servicio" required="required" name="token['+countToken+'][hour]" value="" type="text">' +
                    '</div></div></div><div class="col-sm-6"><div class="form-group"><div class="fg-line">' +
                    '<select class="form-control" title="Tipo de servicio" required="required" name="token['+countToken+'][service]"><option selected="selected" value="">Tipo</option><option value="Entrada">Entrada</option><option value="Salida">Salida</option></select>' +
                    '</div></div></div><div class="col-sm-4"><div class="form-group"><div class="fg-line">' +
                    '<select class="form-control sumatoria " id="programado" required="required" title="El vale estaba programado?" name="token['+countToken+'][schedule]"><option selected="selected" value="">Programado</option><option value="Si">Si</option><option value="No">No</option></select>' +
                    '</div></div></div></div>';
            $('.continuar-after-crear').remove();
            $('#tokens_list').append(rowToken);
        });
    });
</script>


</body>
</html>
