@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Centro de costos             <small>Nuevo</small></h1>
        </div><br>
        <div class="well well-lg">
                <div class="row">
                    @include('centers.form',['center' => $center, 'url' => '/centers', 'method' => 'POST'])
                </div>
            </div>
        </div>


@endsection
