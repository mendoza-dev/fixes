@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Plantas<small></small></h1>
        </div>

        <div class="row">
            <div style="padding-top:50px;"> </div>
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <div align="center">
                                @if($plant->extension)
                                <img class="thumbnail img-responsive" src="{{url("/plants/images/$plant->id.$plant->extension")}}" width="300px" height="300px">
                                @endif
                            </div>
                            <div class="media-body">
                                <hr>
                                <h3><strong>Bio</strong></h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                                <hr>
                                <h3><strong>Razon social</strong></h3>
                                <p>{{ $plant->social_reason }}</p>
                                <hr>
                                <h3><strong>RFC</strong></h3>
                                <p>{{ $plant->rfc }}</p>
                                <hr>
                                <h3><strong>Localizacion</strong></h3>
                                <p>{{ $plant->city }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                    <span>
                        <h1 class="panel-title pull-left" style="font-size:30px;">{{ $plant->name }} <i class="fa fa-check text-success" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="John Doe is sharing with you"></i></h1>
                        <div class="dropdown pull-right">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Acciones
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="{{url('/plants/'.$plant->id.'/edit')}}">Editar</a></li>
                                <li>@include('plants.delete', ['plant' => $plant])</li>
                            </ul>
                        </div>
                    </span>
                       <br><br><hr>
                    <span class="pull-left">
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-fw fa-files-o" aria-hidden="true"></i> {{ $plant->street }}</a>
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-fw fa-picture-o" aria-hidden="true"></i> {{ $plant->ext_number }} </a>
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-fw fa-users" aria-hidden="true"></i> {{ $plant->int_number }} </a>
                    </span>
                    <span class="pull-right">
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-at" aria-hidden="true"></i>{{ $plant->suburb }}</a>
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-envelope-o" aria-hidden="true"></i>{{ $plant->city }}</a>
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-ban" aria-hidden="true"></i>{{ $plant->pc }}</a>
                    </span>
                    </div>
                </div>
                <hr>
                <!-- Simple post content example. -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-left">
                            <a href="#">
                                <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                            </a>
                        </div>
                        <h4><a href="#" style="text-decoration:none;"><strong>{{ $plant->contact_1 }}</strong></a>  <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> </i></a></small></small></h4>
                    <span>

                    </span>
                        <hr>
                        <div class="post-content">
                            <p>{{ $plant->phone_1 }}</p>
                            <p>{{ $plant->mail_1 }}</p>
                        </div>


                    </div>
                </div>
                <!-- Reshare Example -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-left">
                            <a href="#">
                                <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                            </a>
                        </div>
                        <h4><a href="#" style="text-decoration:none;"><strong>{{ $plant->contact_2 }}</strong></a>  <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> </i></a></small></small></h4>
                    <span>

                    </span>
                        <hr>
                        <div class="post-content">
                            <p>{{ $plant->phone_2 }}</p>
                            <p>{{ $plant->mail_2 }}</p>
                        </div>


                    </div>
                </div>


            </div>
        </div>
    </div>


@endsection