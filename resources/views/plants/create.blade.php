@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Plantas                                                    <small>Nueva</small></h1>
        </div><br>
        <div class="well well-lg">
                <div class="row">
                    @include('plants.form',['plant' => $plant, 'url' => '/plants', 'method' => 'POST'])
                </div>
            </div>
            </div>

@endsection
