{!! Form::open (['url' => $url, 'method' => $method , 'files' => true]) !!}
<fieldset>
    <legend class="section" style="color: white;">●  Datos fiscales</legend>

<div class="col-sm-6 ">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('name', $plant->name,['class'=> 'form-control','placeholder'=>'Nombre', 'title'=>"Nombre", 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('social_reason', $plant->social_reason,['class'=> 'form-control ','placeholder'=>'Razon social', 'id' => 'razon','title'=>"Razon social", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('rfc', $plant->rfc,['class'=> 'form-control','placeholder'=>'RFC', 'title'=>"RFC", 'required' => 'required']) }}
        </div>
    </div>
</div>
</fieldset>
<fieldset>
<legend class="section" style="color: white;">●  Datos geograficos</legend>

<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('street', $plant->street,['class'=> 'form-control','placeholder'=>'Calle', 'title'=>"Calle", 'id' => 'address', 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-2">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('ext_number', $plant->ext_number,['class'=> 'form-control','placeholder'=>'No. Exterior', 'title'=>"Numero exterior", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-2">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('int_number',  $plant->int_number,['class'=> 'form-control','placeholder'=>'No. Interior', 'title'=>"Numero interior"]) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('suburb',  $plant->suburb,['class' => 'form-control ', 'placeholder' =>'Colonia', 'id' => 'colonia', 'title'=>"Colonia", 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('city',  $plant->city,['class' => 'form-control', 'placeholder' =>'Ciudad', 'title'=>"Ciudad", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('state', $plant->state,['class' => 'form-control', 'placeholder' =>'Estado', 'title'=>"Estado", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('pc', $plant->pc,['class' => 'form-control', 'placeholder' =>'Codigo postal', 'title'=>"Codigo postal", 'required' => 'required']) }}
        </div>
    </div>
</div>
</fieldset>
<fieldset>
    <legend class="section" style="color: white;">●  Datos de contato</legend>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('contact_1', $plant->contact_1,['class' => 'form-control  ', 'placeholder' =>'Contacto 1', 'id' => 'contact', 'required' => 'required', 'title'=>"Contacto"]) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('phone_1', $plant->phone_1,['class' => 'form-control  ', 'placeholder' =>'Telefono 1', 'id' => 'contact', 'required' => 'required', 'title'=>"Contacto"]) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('mail_1', $plant->mail_1,['class' => 'form-control  ', 'placeholder' =>'Correo 1', 'id' => 'telefono', 'required' => 'required', 'title'=>"Telefono"]) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('contact_2', $plant->contact_2,['class' => 'form-control  ', 'placeholder' =>'Contacto 2', 'id' => 'mail',  'title'=>"Contacto 2"]) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('phone_2', $plant->phone_2,['class' => 'form-control  ', 'placeholder' =>'Telefono 2', 'id' => 'mail', 'title'=>"Telefono  2"]) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('mail_2', $plant->mail_2,['class' => 'form-control  ', 'placeholder' =>'Correo 2', 'id' => 'telefono',  'title'=>"Correo 2"]) }}
        </div>
    </div>
</div>
</fieldset>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::file ('picture', ['title' => 'Imagen']) }}
        </div>
    </div>
</div><br><br>
<hr>



<div class="row">
    <div class="col-md-1 col-md-offset-9" align="">
        <a class="btn btn-warning" href="{{ url('/plants') }}" role="button">Cancelar</a>
    </div>
    <div class="col-sm-1" align="">
        <input class="btn btn-success" type="submit" value="Guardar">
    </div>
</div>

{!! Form::close ()!!}



