{!! Form::open (['url' => $url, 'method' => $method , 'files' => true]) !!}
<fieldset>
    <legend class="section" style="color: white;">●  Datos personales</legend>
<div class="col-sm-6 ">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('name', $driver->name,['class'=> 'form-control','placeholder'=>'Nombre', 'title'=>"Nombre", 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('last_name', $driver->last_name,['class'=> 'form-control ','placeholder'=>'Apellidos', 'id' => 'razon','title'=>"Apellidos", 'required' => 'required']) }}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('street', $driver->street,['class'=> 'form-control','placeholder'=>'Calle', 'title'=>"Calle"]) }}
        </div>
    </div>
</div>

<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('number', $driver->number,['class'=> 'form-control','placeholder'=>'Numero', 'title'=>"Numero", 'id' => 'address']) }}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('suburb', $driver->suburb,['class'=> 'form-control','placeholder'=>'Colonia', 'title'=>"Colonia"]) }}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('city',  $driver->city,['class'=> 'form-control','placeholder'=>'Ciudad', 'title'=>"Ciudad"]) }}
        </div>
    </div>
</div>
<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('state',  $driver->state,['class' => 'form-control ', 'placeholder' =>'Estado', 'id' => 'estado', 'title'=>"Estado"]) }}
        </div>
    </div>
</div>

<div class="col-sm-3">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('phone',  $driver->phone,['class' => 'form-control', 'placeholder' =>'Telefono', 'title'=>"Telefono"]) }}
        </div>
    </div>
</div>
    <div class="col-sm-4">
        <div class="form-group">
            <div class="fg-line">
                {{ Form::file ('picture', ['title' => 'Foto']) }}
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend class="section" style="color: white;">●  Datos operativos</legend>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('date_license', $driver->date_license,['class' => 'form-control', 'placeholder' =>'Vigencia de licencia', 'title'=>"Licencia", 'id' => 'datepicker']) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('number_license', $driver->number_license,['class' => 'form-control', 'placeholder' =>'Numero de licencia', 'title'=>"Numero de licencia"]) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('auto', $driver->auto,['class' => 'form-control  ', 'placeholder' =>'Auto', 'id' => 'car', 'required' => 'required', 'title'=>"Auto"]) }}
        </div>
    </div>
</div>
    <div class="col-sm-4">
        <div class="form-group">
            <div class="fg-line">
                {{ Form::text ('insurance_date', $driver->insurance_date,['class' => 'form-control', 'placeholder' =>'Vigencia de seguro', 'title'=>"Vigencia"]) }}
            </div>
        </div>
    </div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('register', $driver->register,['class' => 'form-control  ', 'placeholder' =>'Numero de registro', 'id' => 'register', 'required' => 'required', 'title'=>"Numero de registro"]) }}
        </div>
    </div>
</div>
    </fieldset>
<fieldset>
    <legend class="section" style="color: white;">●  Datos bancarios</legend>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('bank', $driver->bank,['class' => 'form-control  ', 'placeholder' =>'Banco', 'id' => 'banco', 'title'=>"Banco"]) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('account', $driver->account,['class' => 'form-control  ', 'placeholder' =>'No. de cuenta', 'id' => 'mail', 'title'=>"No. de cuenta"]) }}
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="form-group">
        <div class="fg-line">
            {{ Form::text ('clabe', $driver->clabe,['class' => 'form-control  ', 'placeholder' =>'CLABE', 'id' => 'mail', 'title'=>"CLABE"]) }}
        </div>
    </div>
</div>



</fieldset>
<hr>

<div class="row">
    <div class="col-md-1 col-md-offset-9" align="">
        <a class="btn btn-warning" href="{{ url('/drivers') }}" role="button">Cancelar</a>
    </div>
    <div class="col-sm-1" align="">
        <input class="btn btn-success" type="submit" value="Guardar">
    </div>
</div>

{!! Form::close ()!!}


