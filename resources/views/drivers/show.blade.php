@extends('layouts.app')

@section('content')

    <div class="container" xmlns="http://www.w3.org/1999/html">
        <div class="page-header">
            <h1 style="color: white">Operadores<small></small></h1>
        </div><br>





        <div class="row">
            <div style="padding-top:50px;"> </div>
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <div align="center">
                                @if($driver->extension)
                                    <img class="thumbnail img-responsive" src="{{url("/drivers/images/$driver->id.$driver->extension")}}" width="300px" height="300px">
                                @endif
                            </div>
                            <div class="media-body">
                                <hr>
                                <h3><strong>Registro</strong></h3>
                                <p>{{ $driver->register }}</p>
                                <hr>
                                <h3><strong>Auto</strong></h3>
                                <p>{{ $driver->auto }}</p>
                                <hr>
                                <h3><strong>Vigencia licencia</strong></h3>
                                <p>{{ $driver->date_license }}</p>
                                <hr>
                                <h3><strong>Vigencia seguro</strong></h3>
                                <p>{{ $driver->insurance_date }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                    <span>
                        <h1 class="panel-title pull-left" style="font-size:30px;">{{ $driver->name }} <i class="fa fa-check text-success" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="John Doe is sharing with you"></i></h1>
                        <div class="dropdown pull-right">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Acciones
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="{{url('/drivers/'.$driver->id.'/edit')}}">Editar</a></li>
                                <li>@include('drivers.delete', ['driver' => $driver])</li>
                            </ul>
                        </div>
                    </span>
                        <br><br><hr>
                    <div class="row">
                        <div class="col-sm-3">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-example-modal-lg1">Agregar pago</button></span>
                        <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    Pago
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-3">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-example-modal-lg2">Agregar gasolina</button></span>
                        <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    Gas
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-example-modal-lg3">Actualizar membresia</button></span>
                            <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        Membresia
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target=".bs-example-modal-lg4">Agregar transferencia</button></span>
                            <div class="modal fade bs-example-modal-lg4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        Transferencia
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
                <hr>
                <!-- Simple post content example. -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-left">

                        </div>
                        <h4><a href="#" style="text-decoration:none;"><strong>Datos personales</strong></a>  <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> </i></a></small></small></h4>
                    <span>

                    </span>
                        <hr>
                        <div class="post-content">
                            <p><strong>Calle</strong>  {{ $driver->street }}</p>
                            <p><strong>Numero</strong>  {{ $driver->number }}</p>
                            <p><strong>Fraccionamiento</strong>  {{ $driver->suburb }}</p>
                            <p><strong>Ciudad</strong>  {{ $driver->city }}</p>
                            <p><strong>Estado</strong>  {{ $driver->state }}</p>
                            <p><strong>Telefono</strong>  {{ $driver->phone }}</p>
                        </div>


                    </div>
                </div>
                <!-- Reshare Example -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-left">

                        </div>
                        <h4><a href="#" style="text-decoration:none;"><strong>Datos operativos</strong></a>  <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> </i></a></small></small></h4>
                    <span>

                    </span>
                        <hr>
                        <div class="post-content">
                            <p><strong>Vigencia licencia</strong>  {{ $driver->date_license }}</p>
                            <p><strong>Numero de licencia</strong>  {{ $driver->number_license }}</p>
                            <p><strong>Auto</strong>  {{ $driver->auto }}</p>
                            <p><strong>Vigencia de seguro</strong>  {{ $driver->insurance_date }}</p>
                            <p><strong>Registro</strong>  {{ $driver->register }}</p>

                        </div>


                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-left">

                        </div>
                        <h4><a href="#" style="text-decoration:none;"><strong>Datos bancarios</strong></a>  <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> </i></a></small></small></h4>
                    <span>

                    </span>
                        <hr>
                        <div class="post-content">
                            <p><strong>Banco</strong>  {{ $driver->bank }}</p>
                            <p><strong>Cuenta</strong>  {{ $driver->account }}</p>
                            <p><strong>CLABE</strong>  {{ $driver->clabe }}</p>


                        </div>


                    </div>
                </div>


            </div>
        </div>
    </div>



 @endsection

