@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Operadores               <small>Editar</small></h1>
        </div><br>
        <div class="well well-lg">
                <div class="row">
                    @include('drivers.form',['driver' => $driver, 'url' => '/drivers/'.$driver->id, 'method' => 'PUT'])
                </div>
            </div>
        </div>


@endsection
