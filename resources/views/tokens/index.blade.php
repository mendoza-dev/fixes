@extends('layouts.app')
@section('content')

    <div class="content container">
        <div class="page-header">
            <h1 style="color: white">Vales<small></small></h1>
        </div><br>
        <div class="row">
            <div class="col-sm-3">
                <form action="#" method="get">
                    <div class="input-group">

                        <input class="form-control" id="system-search" name="q" placeholder="Buscar vale" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                    </span>
                    </div>
                </form>
            </div>
            <div class="col-sm-1 col-sm-offset-7"align="center">
                <a href="{{ url('/tokens/create') }}" class="btn btn-info btn-fab">Nuevo vale</a>
            </div>
        </div><br>
        <div class="panel panel-info">
            <div class="panel-heading" style="background-color: #3B5998">Indice </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover table-list-search table-responsive">
                            <thead>
                            <tr>
                                <td>Folio</td>
                                <td>Semana</td>
                                <td>Planta</td>
                                <td>Fecha</td>
                                <td>Zona A</td>
                                <td>Zona B</td>
                                <td>Factura</td>
                                <td>Acciones</td>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tokens as $token)
                                <tr>
                                    <td>{{ $token->id }}</td>

                                    <td>{{ $token->date }}</td>
                                    <td>{{ $token->zones_a }}</td>
                                    <td>{{ $token->enterprises_id }}</td>
                                    <td>{{ $token->driver_id }}</td>

                                    <td><div class="dropdown">
                                            <button id="dLabel" type="button"  class="btn btn-default btn-xs" style="padding: 4px 10px; margin: 1px 1px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Acciones
                                                <span class="caret"></span>
                                            </button>

                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a href="{{url('/tokens/'.$token->id)}}">Ver</a></li>
                                                {{--<li><a href="{{url('/tokens/'.$token->id.'/edit')}}">Editar</a></li>--}}
                                                @if(empty($token->zones_a_id))
                                                    <li>
                                                        {{ link_to_route('tokens-update-base', 'Editar', array('id' => $token->id, 'step' => 2)) }}
                                                    </li>
                                                @elseif(empty($token->zones_b_id))
                                                    <li>
                                                        {{ link_to_route('tokens-update-base', 'Editar', array('id' => $token->id, 'step' => 3)) }}
                                                    </li>
                                                @endif

                                                <li>@include('tokens.delete', ['token' => $token])</li>

                                            </ul>
                                        </div></td>

                                </tr>
                            @endforeach
                            </tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>

    </div>


@endsection