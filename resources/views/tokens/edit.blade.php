@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: #2F70A9">Vales<small></small></h1>
        </div><br>
        <div class="panel panel-primary">
            <div class="panel-heading" style="background-color: #3B5998; color: white">
                <h3 class="panel-title">Editar</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    @include('tokens.form',['token' => $token, 'url' => '/tokens/'.$token->id, 'method' => 'PUT'])
                </div>
            </div>
        </div>
    </div>

@endsection
