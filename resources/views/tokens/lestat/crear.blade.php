@extends('layouts.app')
@section('content')
    <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Asiganacion">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-road"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Zona A">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Zona B">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-barcode"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Completado">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>


            <div class="tab-content">
                <div class="tab-pane active" role="tabpanel" id="step1">
                    <h3>Asignacion</h3>
                    <br>
                    <div class="row">
                        <div class="col-sm-6 ">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::select ('enterprises_id', $enterprises_id, null,['class'=> 'form-control','placeholder'=>'Planta', 'title'=>"Planta ", 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::text ('date', date('Y-m-d'),['class'=> 'form-control','placeholder'=>'Fecha de realizacion', 'title'=>"Fecha de servicio", 'id' => 'datepicker', 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::text ('hour', null,['class'=> 'form-control','placeholder'=>'Hora', 'title'=>"Hora de servicio", 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::select ('service',  $service, null, ['class'=> 'form-control','placeholder'=>'Tipo', 'title'=>"Tipo de servicio", 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::select ('drivers_id',  $drivers_id,null,['class' => 'form-control', 'placeholder' =>'Auto', 'title'=>"Auto de presto el servicio", 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::select ('schedule', $schedule,null,['class' => 'form-control sumatoria ', 'placeholder' =>'Programado', 'id' => 'programado', 'required' => 'required', 'title'=>"El vale estaba programado?", 'required' => 'required']) }}
                                </div>
                            </div>
                        </div>

                    </div>
                    <ul class="list-inline pull-right">
                        <li><input class="btn btn-success next-step" type="submit" value="Guardar y salir"></li>
                        <li><input class="btn btn-success next-step" type="submit" value="Continuar"></li>

                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </section>
@endsection