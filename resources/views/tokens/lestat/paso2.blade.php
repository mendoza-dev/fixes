@extends('layouts.app')
@section('content')
    {!! Form::open (['route' => ['tokens-update-post', $token->id, $step], 'method' => 'post']) !!}
    <section class="container">
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Asiganacion">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-road"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="active">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Zona A">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Zona B">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-barcode"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Completado">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>


            <div class="tab-content">
                <div class="tab-pane" role="tabpanel" id="step1">
                    <h3>Asignacion</h3>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ $token->date }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ $token->hour }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ $token->service }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ $token->schedule }}
                                </div>
                            </div>
                        </div>

                    </div>
                    <ul class="list-inline pull-right">
                        <li><span class="btn btn-success next-step">Continuar</span></li>
                    </ul>
                </div>




                <div class="tab-pane active" role="tabpanel" id="step2">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="fg-line">
                                {{ Form::select ('drivers_id',  $drivers_id,null,['class' => 'form-control', 'placeholder' =>'Auto', 'title'=>"Auto de presto el servicio", 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 ">
                        <div class="form-group">
                            <div class="fg-line">
                                {{ Form::select ('enterprises_id', $enterprises_id, null,['class'=> 'form-control','placeholder'=>'Planta', 'title'=>"Planta ", 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>





                    <h3>Zona A</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::text ('folio', $token->folio,['class'=> 'form-control','placeholder'=>'Folio', 'title'=>"Folio vale fisico"]) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::select ('zones_a_id', $zones_a_id,null,['class'=> 'form-control','placeholder'=>'Zona A', 'title'=>"Zona A"]) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::text ('wait_time_a', $token->wait_time_a,['class' => 'form-control', 'placeholder' =>'Tiempo de espera', 'id' => 'tiempo_a']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="fg-line">
                                    {{ Form::text ('passengers_a',  $token->passengers_a,['class' => 'form-control ', 'placeholder' =>'Pasajeros', 'id' => 'pasajeros_a', 'title'=>"Numero de pasajeros"]) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <a class="btn btn-default" href="javascript:window.print();" role="button">Imprimir vale</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="form-group">
                                <div class="fg-line">
                                    <label>Total a pagar</label>
                                    {{ Form::text ('cost_bill_a',  $token->cost_bill_a,['class' => 'form-control', 'placeholder' =>'Costo de factura', 'id' => 'factura']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 column">
                            <table class="table table-hover" id="tab_logic">
                                <thead>
                                <tr >
                                    <th class="text-center">
                                        #
                                    </th>
                                    <th class="text-center">
                                        Nombre
                                    </th>
                                    <th class="text-center">
                                        Dirección
                                    </th>
                                    <th class="text-center">
                                        Telefono
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id='addr0'>
                                    <td>
                                        1
                                    </td>
                                    <td>
                                        <input type="text" name='name0'  placeholder='Nombre' class="form-control"/>
                                    </td>
                                    <td>
                                        <input type="text" name='mail0' placeholder='Dirección' class="form-control"/>
                                    </td>
                                    <td>
                                        <input type="text" name='mobile0' placeholder='Telefono' class="form-control"/>
                                    </td>
                                </tr>
                                <tr id='addr1'></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1" >
                            <a id="add_row" class="btn btn-success ">Agregar</a>
                        </div>
                        <div class="col-sm-1" >
                            <a id='delete_row' class="btn btn-warning">Borrar</a>
                        </div>
                    </div>
                    <hr>

                    <ul class="list-inline pull-right">
                        <li><button type="button" class="btn btn-default prev-step">Regresar</button></li>
                        <li><input type="submit" class="btn btn-primary" name="accion_token" value="Guardar"></li>
                        <li><input type="submit" class="btn btn-primary" name="accion_token" value="Continuar"></li>
                    </ul>
                </div>
                <div class="tab-pane" role="tabpanel" id="step3">
                    <h3>Zona B</h3>

                    <ul class="list-inline pull-right">
                        <li><button type="button" class="btn btn-default prev-step">Regresar</button></li>

                        <li><button type="button" class="btn btn-primary btn-info-full next-step">Guardar</button></li>
                    </ul>
                </div>
                <div class="tab-pane" role="tabpanel" id="complete">
                    <h3>Completado</h3>
                    <p>Vale guardado exitosamente</p>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </section>
@endsection