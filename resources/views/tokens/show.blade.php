@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Vales<small></small></h1>
        </div><br>
        <div class="row">


            <div class="col-sm-1 "align="center">
                <a href="{{ url('/tokens') }}" class="btn btn-info btn-fab">Regresar</a>
            </div>

        </div><br>
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #3B5998; color: white">
                <h3 class="panel-title">Folio {{$token->folio}}</h3>

            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        Planta <span class="label label-info">{{ $token->plants_id }}</span>
                    </div>
                    <div class="col-sm-3">
                        Semana <span class="label label-info">{{ $token->weeks_id }}</span>
                    </div>
                    <div class="col-sm-3">
                        Fecha <span class="label label-info">{{ $token->date }}</span>
                    </div>
                    <div class="col-sm-3">
                        Hora <span class="label label-info">{{ $token->hour }}</span>
                    </div>
                    <div class="col-sm-3">
                        Tipo <span class="label label-info">{{ $token->service }}</span>
                    </div>
                    <div class="col-sm-3">
                        Auto <span class="label label-info">{{ $token->drivers_id }}</span>
                    </div>
                    <div class="col-sm-3">
                        Pasajeros <span class="label label-info">{{ $token->passengers }}</span>
                    </div>
                    <div class="col-sm-3">
                        Colonia <span class="label label-info">{{ $token->suburb }}</span>
                    </div>
                    <div class="col-sm-3">
                        Zona A <span class="label label-info">{{ $token->zones_a }}</span>
                    </div>
                    <div class="col-sm-3">
                        Zona B <span class="label label-info">{{ $token->zones_b_id }}</span>
                    </div>
                    <div class="col-sm-3">
                        Tiempo de espera <span class="label label-info">{{ $token->wait_time }}</span>
                    </div>
                    <div class="col-sm-3">
                        Otro costo  <span class="label label-info">{{ $token->zones_b_id }}</span>
                    </div>
                    <div class="col-sm-3">
                        Programado <span class="label label-info">{{ $token->schedule }}</span>
                    </div>
                    <div class="col-sm-3">
                        Centro de servicio <span class="label label-info">{{ $token->centers_id }}</span>
                    </div>
                    <div class="col-sm-3">
                        Otro costo <span class="label label-info">{{ $token->other_cost }}</span>
                    </div>
                    <div class="col-sm-3">
                        Concepto <span class="label label-info">{{ $token->concept }}</span>
                    </div>
                    <div class="col-sm-3">
                        Total <span class="label label-info">{{ $token->cost_bill }}</span>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

@endsection