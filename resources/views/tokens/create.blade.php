@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="page-header">
            <h1 style="color: white">Vale<small></small></h1>
        </div><br>
        <div class="panel panel-primary">
                   <div class="panel-heading" style="background-color: #3B5998; color: white">
                <h3 class="panel-title">Nuevo</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    @include('tokens.form',['token' => $token, 'url' => '/tokens', 'method' => 'POST'])
                </div>
            </div>
        </div>



    </div>



    </div>

@endsection
