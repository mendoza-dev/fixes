@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="jumbotron">
        <div class="row">
            {{ $tokens->count() }}
        </div>
        </div>
        <div id="piechart" style="width: 1140px; height: 500px; background-color: #272b30"></div>
    </div>

@endsection
