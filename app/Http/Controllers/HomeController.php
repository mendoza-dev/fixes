<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Token;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $tokens = Token::select('tokens.plants_id')
            ->selectRaw('count(tokens.id) as tokens_count')
            ->selectRaw('sum(tokens.cost_bill) as total')
            ->leftjoin('plants', 'plants.id', '=', 'tokens.plants_id')
            ->groupBy('tokens.plants_id');

        return view('home', ['tokens' => $tokens->get()]);


    }
}