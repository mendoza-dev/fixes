<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Zone_a;
use App\Enterprise;

class ZonesaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $zonesa = Zone_a::all();
        return view('zonesa.index', ['zonesa' => $zonesa]);
    }


    public function create()
    {
        $zonea = new Zone_a;
        return view ('zonesa.create', ['zonea' => $zonea, 'enterprises_id' => Enterprise::pluck('name', 'id')]);

        $zonea->enterprise();
    }


    public function store(Request $request)
    {
        $zonea = new Zone_a;

        $zonea -> name = $request ->name;
        $zonea -> value = $request -> value;
        $zonea -> enterprises_id = $request ->enterprises_id;

        $zonea->all();
        $zonea->enterprise();

        if ($zonea->save()){
            return redirect('/zonesa/create');}
        else {
            return view('/503');
        }

        
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $zonea = Zone_a::find($id);
        return view('zonesa.edit', ['zonea' => $zonea, 'enterprises_id' => Enterprise::pluck('name', 'id')]);
    }


    public function update(Request $request, $id)
    {
        $zonea = Zone_a::find($id);
        
        $zonea -> name = $request ->name;
        $zonea -> value = $request -> value;
        $zonea -> enterprises_id = $request ->enterprises_id;

        $zonea->all();
        $zonea->enterprise();

        if ($zonea->save()){
            return redirect('/zonesa');}
        else {
            return view('zonesa.edit');
        }
    }


    public function destroy($id)
    {
        Zone_a::destroy($id);
        return redirect('/zonesa');
    }
}
