<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Plant;
use App\Token;
use App\Week;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoicesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $invoices = Week::select('weeks.*', 'tokens.plants_id', 'plants.name as planta_nombre')
            ->selectRaw('count(tokens.id) as tokens_count')
            ->selectRaw('sum(tokens.cost_bill) as total')
            ->join('tokens', 'tokens.weeks_id', '=', 'weeks.id')
            ->leftjoin('plants', 'plants.id', '=', 'tokens.plants_id')
            ->groupBy('tokens.plants_id', 'tokens.weeks_id')
            ->where('weeks.date_end', '<=', date('Y-m-d H:i:s'));
        //dd($invoices->get());
        return view('invoices.index', ['invoices' => $invoices->get()]);

    }



    public function edit_by_plant($weeks_id, $plants_id){
        $tokens = Token::where('weeks_id', $weeks_id)->where('plants_id', $plants_id)->get();
        return view('invoices.create')->with(array(
            'tokens' => $tokens,
            'plant' => Plant::where('id', $plants_id)->first(),
            'weeks_id' => $weeks_id,
            
        ));
    }

    public function update_by_plant(Request $request, $weeks_id, $plants_id){
        //dd($request->all());
        $tokens = Token::where('weeks_id', $weeks_id)->where('plants_id', $plants_id);
        $invoice_id = clone $tokens;
        $invoice_data = array(
            'date' => date('Y-m-d'),
            'mount' => $tokens->get()->sum('cost_bill'),
            'plants_id' => $plants_id,
            'weeks_id' => $weeks_id
        );

        if(!empty($invoice_id->first()->invoice_id)){
            Invoice::where('id', $invoice_id->first()->invoice_id)->update($invoice_data);
            $invoice_id = $invoice_id->first()->invoice_id;
        }else{
            $invoice = Invoice::create($invoice_data);
            $invoice_id = $invoice->id;
        }

        $tokens->update(['invoice_id' =>  $invoice_id]);

        return redirect()->route('invoices.index');
    }


    public function create()
    {
        
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        return view('invoices.show')->with(array(
            'invoice' => Invoice::where('id', $id)->first()
        ));
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
