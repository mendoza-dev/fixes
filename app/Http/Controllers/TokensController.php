<?php

namespace App\Http\Controllers;



//use Dotenv\Validator;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Week;
use App\Token;
use App\Zone_b;
use App\Driver;
use App\Plant;
use App\Center;
use App\Zone_a;
use App\Enterprise;
use App\Mail\TokensCreate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Validator;

class TokensController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {




        $tokens = Token::all();
        return view('tokens.index',["tokens" => $tokens]);

    }


    /*
    * Funcion para invocar template de formulario
    */
    public function tokens_crear_base(){
        return view('tokens.lestat.paso1')->with(array(
            "service" => ['Entrada' => 'Entrada', 'Salida' => 'Salida'],
            "schedule" => ['Si' => 'Si', 'No' => 'No']
        ));
    }

    public function tokens_crear_post(Request $request){
        $validar = Validator::make($request->all(), array(
            'token.*.date' => 'required|date',
            'token.*.hour' => 'required',
            'token.*.service' => 'required|in:Entrada,Salida',
            'token.*.schedule' => 'required|in:Si,No'
        ));

        if($validar->fails()){
            return redirect()->back()->withErrors($validar->errors())->withInput();
        }

        foreach($request->token as $item){
            $token = Token::create($item);
        }

        //$token = Token::create($request->only('enterprises_id', 'date', 'hour', 'service', 'drivers_id', 'schedule'));

        if(empty($request->evento_token) || $request->evento_token == 'Crear'){
            return redirect()->route('tokens.index');
        }else{
            return redirect()->route('tokens-update-base', ['id' => $token->id, 'step' => 2]);
        }
    }

    public function tokens_update_base($id, $step){
        return view('tokens.lestat.paso'.$step)->with(array(
            'step' => $step,
            'token' => Token::where('id', $id)->first(),
            'zones_a_id' => Zone_a::pluck('name', 'id'),
            "drivers_id" => Driver::pluck ('register','id'),
            "enterprises_id" => Enterprise::pluck ('name', 'id'),
            "service" => ['Entrada' => 'Entrada', 'Salida' => 'Salida'],
            "schedule" => ['Si' => 'Si', 'No' => 'No'],
            "weeks_id" =>Week::pluck('number','id'),
            "plants_id" => Plant::pluck ('name','id'),
            "centers_id" => Center::pluck ('name','id'),
            "zones_b_id" => Zone_b::pluck('value', 'id'),
            "zones_b_json" => Zone_b::pluck('value', 'id')->toJson(),
        ));
    }

    public function tokens_update_post(Request $request, $id, $step){
        $token = Token::where('id', $id)->update($request->only('enterprises_id', 'passengers_a', 'wait_time_a', 'cost_bill_a', 'drivers_id', 'zones_a_id', 'wait_time_a'));
        if(empty($token->accion_token) || $token->accion_token == 'Guardar'){
            return redirect()->route('tokens.index');
        }else{
            return redirect()->route('tokens-update-base', ['id' => $token->id, 'step' => 3]);
        }
    }

    public function create()
    {
        $token = new Token;

        return view("tokens.create", [
            "token" => $token, "zones_b_id" => Zone_b::get(),
            "zones_b_json" => Zone_b::pluck('value', 'id')->toJson(),
            "plants_id" => Plant::pluck ('name','id'),
            "centers_id" => Center::pluck ('name','id'),
            "drivers_id" => Driver::pluck ('register','id'),
            "schedule" => ['Si' => 'Si', 'No' => 'No'],
            "service" => ['Entrada' => 'Entrada', 'Salida' => 'Salida'],
            "weeks_id" =>Week::pluck('number','id'),
            "enterprises_id" => Enterprise::pluck ('name', 'id'), 'zones_a_id' => Zone_a::get()]);

        $token->driver();
        $token->plant();
        $token->week();
        $token->center();
        $token->zoneb();
        $token->all();
        $token->zonea();
    }



    public function store(Request $request)
    {
        $token = new Token;

        if(!empty($request->token_id)){
            $token->where('id', $request->token_id);
        }

        switch(true){
            case empty($request->token_id):

                $token->folio = $request->folio;
                $token->date = $request->date;
                $token->hour = $request->hour;
                $token->schedule = $request->schedule;
                $token->service = $request->service;
                $token->user_id = Auth::user()->id;

                break;
            case !empty($request->token_id) && !empty($request->enterprises_id):

                $token->enterprises_id = $request->enterprises_id;
                $token->passengers_a = $request->passengers_a;
                $token->wait_time_a = $request->wait_time_a;
                $token->cost_bill_a = $request->cost_bill_a;
                $token->suburb = $request->suburb;
                $token->drivers_id = $request->drivers_id;
                $token->zones_a_id = $request->zones_a_id;
                $token->wait_time_a= $request->wait_time_a;

                break;
            case !empty($request->token_id) && !empty($request->weeks_id    ):

                $token->weeks_id = $request->weeks_id;
                $token->plants_id = $request->plants_id;
                $token->centers_id = $request->centers_id;
                $token->passengers = $request->passengers;
                $token->zones_b_id = $request->zones_b_id;
                $token->wait_time = $request->wait_time;
                $token->other_cost = $request->other_cost;
                $token->concept = $request->concept;
                $token->cost_bill = $request->cost_bill;

                break;
            default:

                break;
        }

        /*$token->zoneb();
        $token->plant();
        $token->driver();
        $token->all();*/

        if ($token->save()){
            return redirect("/tokens/create");}
        else{
            return view ("/503");
        }
    }


    public function show($id)
    {
        $token = Token::find($id);
        return view('tokens.show', ['token' => $token]);
    }


    public function edit($id)
    {
        $token = Token::find($id);
        return view("tokens.edit") ->with (["token" => $token, "zones_b_id" => Zone_b::get(), "zones_b_json" => Zone_b::pluck('value', 'id')->toJson(),
            "plants_id" => Plant::pluck ('name','id'), "centers_id" => Center::pluck ('name','id'), "drivers_id" => Driver::pluck ('register','id'),
            "schedule" => ['Si' => 'Si', 'No' => 'No'], "service" => ['Entrada' => 'Entrada', 'Salida' => 'Salida'], "weeks_id" =>Week::pluck('number','id'),
            "enterprises_id" => Enterprise::pluck ('name', 'id'), 'zones_a_id' => Zones_A::get()]);



        $token->driver();
        $token->plant();
        $token->week();
        $token->center();
        $token->zoneb();
        $token->all();
    }


    public function update(Request $request, $id)
    {
        $token = Token::find($id);

        $token->folio = $request->folio;
        $token->week = $request->week;
        $token->plants_id = $request->plants_id;
        $token->date = $request->date;
        $token->hour = $request->hour;
        $token->service = $request->service;
        $token->centers_id = $request->centers_id;
        $token->zones_a = $request->zones_a;
        $token->schedule = $request->schedule;
        $token->passengers = $request->passengers;
        $token->suburb = $request->suburb;
        $token->zones_b_id = $request->zones_b_id;
        $token->wait_time = $request->wait_time;
        $token->drivers_id = $request->drivers_id;
        $token->other_cost = $request->other_cost;
        $token->concept = $request->concept;
        $token->cost_bill = $request->cost_bill;

        if ($token->save()){
            return redirect("/tokens");}
        else{
            return view ("tokens.edit");
        }

        $token->driver();
        $token->plant();
        $token->center();
        $token->zoneb();
        $token->all();

    }


    public function destroy($id)
    {
        Token::destroy($id);
        return redirect('/tokens');
    }



}
