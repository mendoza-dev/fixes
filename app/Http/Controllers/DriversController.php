<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Gas;
use App\Payment;
use App\Membership;
use App\Transfer;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DriversController extends Controller
{

    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function index()
    {
        $drivers = Driver::all();
        return view('drivers.index',["drivers" => $drivers]);
    }


    public function create()
    {
        $driver = new Driver();
        return view("drivers.create", ["driver" => $driver, 'transfers_id' => Transfer::pluck('folio', 'id'),
        'payments_id' => Payment::pluck('folio', 'id'), 'memberships_id' => Membership::pluck('date', 'id'),
        'gas_id' => Gas::pluck('folio', 'id')]);

        $driver->payment();
        $driver->transfer();
        $driver->membership();
        $driver->gas();       
        
    }


    public function store(Request $request)
    {
        $driver = new Driver();
        
        $hasFile = $request->hasFile('picture') && $request->picture->isValid();
        
        $driver->name = $request->name;
        $driver->last_name = $request->last_name;
        $driver->street = $request->street;
        $driver->number = $request->number;
        $driver->suburb = $request->suburb;
        $driver->city = $request->city;
        $driver->state = $request->state;
        $driver->phone = $request->phone;

        $driver->date_license = $request->date_license;
        $driver->number_license = $request->number_license;
        $driver->auto = $request->auto;
        $driver->insurance_date = $request->insurance_date;
        $driver->register = $request->register;

        $driver->bank = $request->bank;
        $driver->account = $request->account;
        $driver->clabe = $request->clabe;
        $driver->extension = $request->extension;

        $driver->transfers_id = $request->transfers_id;
        $driver->payments_id = $request->payments_id;
        $driver->memberships_id = $request->memberships_id;
        $driver->gas_id = $request->gas_id;

        $driver->user_id = Auth::user()->id;        

        if ($hasFile){
            $extension = $request->picture->extension();
            $driver->extension = $extension;
        }

        if ($driver->save()){

            if($hasFile){
                $request->picture->storeAs('images', "$driver->id.$extension");
            }
            return redirect("/drivers");}
        else{
            return view ("/503");
        }
    }


    public function show($id)
    {
        $driver = Driver::find($id);
        return view('drivers.show', ['driver' => $driver]);
    }


    public function edit($id)
    {
        $driver = Driver::find($id);
        return view("drivers.edit", ["driver" => $driver, 'transfers_id' => Transfer::pluck('folio', 'id'),
            'payments_id' => Payment::pluck('folio', 'id'), 'memberships_id' => Membership::pluck('date', 'id'),
            'gas_id' => Gas::pluck('folio', 'id')]);

        $driver->payment();
        $driver->transfer();
        $driver->membership();
        $driver->gas();

    }   
    

    public function update(Request $request, $id)
    {
        $driver = Driver::find($id);

        $hasFile = $request->hasFile('picture') && $request->picture->isValid();

        $driver->name = $request->name;
        $driver->last_name = $request->last_name;
        $driver->street = $request->street;
        $driver->number = $request->number;
        $driver->suburb = $request->suburb;
        $driver->city = $request->city;
        $driver->state = $request->state;
        $driver->phone = $request->phone;

        $driver->date_license = $request->date_license;
        $driver->number_license = $request->number_license;
        $driver->auto = $request->auto;
        $driver->insurance_date = $request->insurance_date;
        $driver->register = $request->register;

        $driver->bank = $request->bank;
        $driver->account = $request->account;
        $driver->clabe = $request->clabe;
        $driver->extension = $request->extension;

        $driver->transfers_id = $request->transfers_id;
        $driver->payments_id = $request->payments_id;
        $driver->memberships_id = $request->memberships_id;
        $driver->gas_id = $request->gas_id;


        if ($hasFile){
            $extension = $request->picture->extension();
            $driver->extension = $extension;
        }

        if ($driver->save()){

            if($hasFile){
                $request->picture->storeAs('images', "$driver->id.$extension");
            }
            return redirect("/drivers");}
        else{
            return view ("/drivers/edit");
        }
    }


    public function destroy($id)
    {
        Driver::destroy($id);
        return redirect('/drivers');
    }
}
