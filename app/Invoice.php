<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public $fillable = array(
        'date',
        'mount',
        'plants_id',
        'weeks_id'
    );

    public function getTokens(){
        return $this->hasMany('App\Token', 'invoice_id', 'id');
    }
}
