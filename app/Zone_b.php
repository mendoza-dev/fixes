<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone_b extends Model
{
    protected $table = 'zones_b';
    
    public function plant(){
        
        return $this->hasOne('App\Plant');
    }
}
