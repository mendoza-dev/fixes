<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{

    protected $table = 'drivers';


    public function payment(){
        return $this->hasMany('App\Payment');
    }

    public function membership(){
        return $this->hasMany('App\Membership');
    }

    public  function gas(){
        return $this->hasMany('App\Gas');
    }

    public function transfer(){
        return $this->hasMany('App\Transfer');
    }

    
}
