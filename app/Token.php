<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Zone_b;
use App\Zone_A;

use App\Mail\TokensCreate;
use Illuminate\Support\Facades\Mail;

class Token extends Model
{
    protected $table = 'tokens';

    public $fillable = array(
        'enterprises_id',
        'date',
        'hour',
        'service',
        'drivers_id',
        'schedule',
    );


    public function zoneb()
    {
        return $this->hasOne('App\Zone_b');
    }

    public function zonea()
    {
        return $this->hasOne('App\Zone_a', 'id', 'zones_a_id');
    }

    public function driver(){

        return $this->hasOne('App\Driver', 'id', 'drivers_id');
    }

    public function plant(){
        return $this->hasOne('App\Plant', 'id', 'plants_id');
    }

    public function enterprise(){
        return $this->hasOne('App\Enterprise', 'id', 'enterprises_id');
    }

    public function center(){
        
        return $this->hasOne('App\Center');
    }
    
    public function week(){
        
        return $this->hasOne('App\Week');
    }

    public function scopeLatest($query){
        return $query->orderBy('id', 'desc');
    }

    public function getZoneB(){

        return $this->hasOne('App\Zone_b', 'id', 'zones_b_id');
    }

    public function sendMail(){

        Mail::to('jemg1001@gmail.com')->send(new TokensCreate());
    }

    public function getZoneA(){

        return $this->hasOne('App\Zone_a', 'id', 'zones_b_id');
    }



    
    
}
