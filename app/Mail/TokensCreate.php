<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TokensCreate extends Mailable
{
    use Queueable, SerializesModels;


    public $token;
    public $tokens;

    public function __construct($token)
    {
        $this->token = $token;
        $this->tokens =[];

    }


    public function build()
    {
        return $this->from ('juanpi.mendoza@gmail.com')
            ->view('mailers.token');
    }
}
