<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone_a extends Model
{
    protected $table = 'zones_a';
    
    
    public function enterprise(){
        
        return $this->hasOne('App\Enterprise');
    }
}
