<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('travelers', function (Blueprint $table){
           $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('street');
            $table->string('number');
            $table->string('suburb');
            $table->string('city');
            $table->string('phone');
            $table->float('payment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
