<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('invoices', function (Blueprint $table){
            $table->increments('id');
            $table->date('date');
            $table->float('mount');

            $table->integer('weeks_id')->unsigned();
            $table->integer('plants_id')->unsigned();

            $table->foreign('weeks_id')->references('id')->on('weeks');
            $table->foreign('plants_id')->references('id')->on('plants');


            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
