<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments("id");
            $table->integer('user_id')->unsigned();
            $table->string('brand');
            $table->string('model');
            $table->string('year');
            $table->string('enrollment');
            $table->string('serial');
            $table->date('insurance');
            $table->date('previous_maintenance');
            $table->date('later_maintenance');
            $table->string('extension')->nullable();

            $table->integer('kilometers_id')->unsigned()->nullable();
            $table->integer('maintenances_id')->unsigned()->nullable();
            $table->integer('operators_id')->unsigned()->nullable();
            $table->integer('personals_id')->unsigned()->nullable();


            $table->foreign('kilometers_id')->references('id')->on('kilometers');
            $table->foreign('personals_id')->references('id')->on('personals');
            $table->foreign('maintenances_id')->references('id')->on('maintenances');
            $table->foreign('operators_id')->references('id')->on('operators');

            $table->timestamps();

            
            
        });
    }

    
    public function down()
    {
        //
    }
}
