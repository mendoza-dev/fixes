<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokens', function (Blueprint $table){

            $table->increments("id");
            $table->string('folio')->unique()->nullable();
            $table->date('date');
            $table->string('hour');
            $table->string('client');
            $table->enum('service',['Entrada', 'Salida'] );
            $table->enum('schedule',['Si', 'No']);
            
            $table->float('passengers_a')->nullable();
            $table->float('wait_time_a')->nullable();
            $table->float('cost_bill_a')->nullable();
            $table->string('suburb')->nullable();

            $table->float('passengers')->nullable();
            $table->float('wait_time')->nullable();
            $table->float('other_cost')->nullable();
            $table->string('concept')->nullable();
            $table->float('cost_bill')->nullable();

            $table->integer('user_id')->unsigned();
            $table->integer('zones_b_id')->unsigned()->nullable();
            $table->integer('zones_a_id')->unsigned()->nullable();
            $table->integer('plants_id')->unsigned()->nullable();
            $table->integer('enterprises_id')->unsigned()->nullable();
            $table->integer('centers_id')->unsigned()->nullable();
            $table->integer('drivers_id')->unsigned()->nullable();
            $table->integer('weeks_id')->unsigned()->nullable();
            $table->integer('invoice_id')->unsigned()->nullable();

            $table->foreign('zones_b_id')->references('id')->on('zones_b');
            $table->foreign('zones_a_id')->references('id')->on('zones_a');
            $table->foreign('plants_id')->references('id')->on('plants');
            $table->foreign('enterprises_id')->references('id')->on('enterprises');
            $table->foreign('centers_id')->references('id')->on('centers');
            $table->foreign('drivers_id')->references('id')->on('drivers');
            $table->foreign('weeks_id')->references('id')->on('weeks');
            

            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tokens');
    }
}
