<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('social_reason');
            $table->string('rfc');
            $table->string('street');
            $table->string('ext_number');
            $table->string('int_number')->nullable();
            $table->string('suburb');
            $table->string('city');
            $table->string('state');
            $table->string('pc');
            $table->string('contact_1');
            $table->string('contact_2')->nullable();
            $table->string('mail_1');
            $table->string('mail_2')->nullable();
            $table->string('phone_1');
            $table->string('phone_2')->nullable();
            $table->string('extension')->nullable(); 
            
            $table->timestamps();

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plants');
    }
}
