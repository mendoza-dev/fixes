<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ('gas', function (Blueprint $table){

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('folio')->unique();
            $table->float('mount');
            $table->date('date');

            $table->timestamps();

           
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gas');
    }
}
