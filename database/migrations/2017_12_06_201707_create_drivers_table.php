<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table){

            $table->increments('id');
            $table->integer('user_id')->unsigned();

            $table->string('name');
            $table->string('last_name');
            $table->string('street');
            $table->string('number');
            $table->string('suburb');
            $table->string('city');
            $table->string('state');
            $table->string('phone');

            $table->date('date_license');
            $table->string('number_license');
            $table->string('auto');
            $table->date('insurance_date');
            $table->string('register')->unique();

            $table->string('bank');
            $table->string('account');
            $table->string('clabe');
            $table->string('extension')->nullable();

            $table->integer('transfers_id')->unsigned()->nullable();
            $table->integer('payments_id')->unsigned()->nullable();
            $table->integer('memberships_id')->unsigned()->nullable();
            $table->integer('gas_id')->unsigned()->nullable();

            $table->foreign('transfers_id')->references('id')->on('transfers');
            $table->foreign('payments_id')->references('id')->on('payments');
            $table->foreign('memberships_id')->references('id')->on('memberships');
            $table->foreign('gas_id')->references('id')->on('gas');

            $table->timestamps();



            


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drivers');
    }
}
